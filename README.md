# Scratch project

This project is not part of the Tundra Wolf project. It is used to experiment with Rust crates.

## Packages

### Winnow (`parser/winnow_e`)

Experiments with the [winnow](https://docs.rs/winnow/latest/winnow/index.html) parser combinators crate. It contains the following binaries.

* winnow_1 - Iterator combinator example
* winnow_2 - Do nothing parser
* winnow_3 - Character and slice parsers
