use winnow::ascii::{digit1, hex_digit1, oct_digit1};
use winnow::combinator::{alt, dispatch, fail};
use winnow::token::{take, take_while};
use winnow::{PResult, Parser};

// Using `take_while`
pub fn parse_bin_digits<'s>(input: &mut &'s str) -> PResult<&'s str> {
    take_while(1.., '0'..='1').parse_next(input)
}

// Using `digit1`
pub fn parse_dec_digits<'s>(input: &mut &'s str) -> PResult<&'s str> {
    digit1.parse_next(input)
}

// Using `hex_digit1`
pub fn parse_hex_digits<'s>(input: &mut &'s str) -> PResult<&'s str> {
    hex_digit1.parse_next(input)
}

// Using `oct_digit1`
pub fn parse_oct_digits<'s>(input: &mut &'s str) -> PResult<&'s str> {
    oct_digit1.parse_next(input)
}

// Alternative using `alt`
pub fn parse_digits<'s>(input: &mut &'s str) -> PResult<(&'s str, &'s str)> {
    alt((
        ("0b", parse_bin_digits),
        ("0o", parse_oct_digits),
        ("0d", parse_dec_digits),
        ("0x", parse_hex_digits),
    ))
    .parse_next(input)
}

// Custom output type using `try_map`
pub fn parse_as_usize(input: &mut &str) -> PResult<usize> {
    dispatch!(take(2usize);
        "0b" => parse_bin_digits.try_map(|s| usize::from_str_radix(s, 2)),
        "0o" => parse_oct_digits.try_map(|s| usize::from_str_radix(s, 8)),
        "0d" => parse_dec_digits.try_map(|s| usize::from_str_radix(s, 10)),
        "0x" => parse_hex_digits.try_map(|s| usize::from_str_radix(s, 16)),
        _ => fail,
    )
    .parse_next(input)
}
