use winnow::combinator::{dispatch, fail, opt};
use winnow::stream::Stream;
use winnow::token::take;
use winnow::{PResult, Parser};
use winnow_e::{
    parse_bin_digits, parse_dec_digits, parse_digits, parse_hex_digits, parse_oct_digits,
};

// Alternative using `checkpoint` and `reset`
fn parse_digits_1<'s>(input: &mut &'s str) -> PResult<(&'s str, &'s str)> {
    let start = input.checkpoint();

    if let Ok(output) = ("0b", parse_bin_digits).parse_next(input) {
        return Ok(output);
    }

    input.reset(start);
    if let Ok(output) = ("0o", parse_oct_digits).parse_next(input) {
        return Ok(output);
    }

    input.reset(start);
    if let Ok(output) = ("0d", parse_dec_digits).parse_next(input) {
        return Ok(output);
    }

    input.reset(start);
    ("0x", parse_hex_digits).parse_next(input)
}

// Alternative using `opt`
fn parse_digits_2<'s>(input: &mut &'s str) -> PResult<(&'s str, &'s str)> {
    if let Some(output) = opt(("0b", parse_bin_digits)).parse_next(input)? {
        Ok(output)
    } else if let Some(output) = opt(("0o", parse_oct_digits)).parse_next(input)? {
        Ok(output)
    } else if let Some(output) = opt(("0d", parse_dec_digits)).parse_next(input)? {
        Ok(output)
    } else {
        ("0x", parse_hex_digits).parse_next(input)
    }
}

// Alternative using `dispatch`, `fail` and `take`
fn parse_digits_4<'s>(input: &mut &'s str) -> PResult<&'s str> {
    dispatch!(
        take(2usize);
        "0b" => parse_bin_digits,
        "0o" => parse_oct_digits,
        "0d" => parse_dec_digits,
        "0x" => parse_hex_digits,
        _ => fail,
    )
    .parse_next(input)
}

fn main() {
    println!("{} - alternatives", env!("CARGO_BIN_NAME"));

    // Using `checkpoint` and `reset`
    let mut input = "0x1a2b Hello";

    let (prefix, digits) = parse_digits_1.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(prefix, "0x");
    assert_eq!(digits, "1a2b");

    let mut input = "0b101b Hello";

    let (prefix, digits) = parse_digits_1.parse_next(&mut input).unwrap();

    assert_eq!(input, "b Hello");
    assert_eq!(prefix, "0b");
    assert_eq!(digits, "101");

    let mut input = "0o173b Hello";

    let (prefix, digits) = parse_digits_1.parse_next(&mut input).unwrap();

    assert_eq!(input, "b Hello");
    assert_eq!(prefix, "0o");
    assert_eq!(digits, "173");

    let mut input = "0d1938 Hello";

    let (prefix, digits) = parse_digits_1.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(prefix, "0d");
    assert_eq!(digits, "1938");

    assert!(parse_digits_1(&mut "ghiworld").is_err());

    // Using `opt`
    let mut input = "0x1a2b Hello";

    let (prefix, digits) = parse_digits_2.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(prefix, "0x");
    assert_eq!(digits, "1a2b");

    let mut input = "0b101b Hello";

    let (prefix, digits) = parse_digits_2.parse_next(&mut input).unwrap();

    assert_eq!(input, "b Hello");
    assert_eq!(prefix, "0b");
    assert_eq!(digits, "101");

    let mut input = "0o173b Hello";

    let (prefix, digits) = parse_digits_2.parse_next(&mut input).unwrap();

    assert_eq!(input, "b Hello");
    assert_eq!(prefix, "0o");
    assert_eq!(digits, "173");

    let mut input = "0d1938 Hello";

    let (prefix, digits) = parse_digits_2.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(prefix, "0d");
    assert_eq!(digits, "1938");

    assert!(parse_digits_2(&mut "ghiworld").is_err());

    // Using `alt`
    let mut input = "0x1a2b Hello";

    let (prefix, digits) = parse_digits.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(prefix, "0x");
    assert_eq!(digits, "1a2b");

    let mut input = "0b101b Hello";

    let (prefix, digits) = parse_digits.parse_next(&mut input).unwrap();

    assert_eq!(input, "b Hello");
    assert_eq!(prefix, "0b");
    assert_eq!(digits, "101");

    let mut input = "0o173b Hello";

    let (prefix, digits) = parse_digits.parse_next(&mut input).unwrap();

    assert_eq!(input, "b Hello");
    assert_eq!(prefix, "0o");
    assert_eq!(digits, "173");

    let mut input = "0d1938 Hello";

    let (prefix, digits) = parse_digits.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(prefix, "0d");
    assert_eq!(digits, "1938");

    assert!(parse_digits(&mut "ghiworld").is_err());

    // Using `dispatch`, `fail` and `take`
    let mut input = "0x1a2b Hello";

    let digits = parse_digits_4.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(digits, "1a2b");

    let mut input = "0b101b Hello";

    let digits = parse_digits_4.parse_next(&mut input).unwrap();

    assert_eq!(input, "b Hello");
    assert_eq!(digits, "101");

    let mut input = "0o173b Hello";

    let digits = parse_digits_4.parse_next(&mut input).unwrap();

    assert_eq!(input, "b Hello");
    assert_eq!(digits, "173");

    let mut input = "0d1938 Hello";

    let digits = parse_digits_4.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(digits, "1938");

    assert!(parse_digits_4(&mut "ghiworld").is_err());
}
