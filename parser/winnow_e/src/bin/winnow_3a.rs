use winnow::ascii::hex_digit1;
use winnow::error::{ErrMode, ErrorKind, ParserError};
use winnow::stream::Stream;
use winnow::token::{any, one_of, take_while};
use winnow::{PResult, Parser};

//
// Single character parsers
//

// Using core components
pub fn parse_prefix_1(input: &mut &str) -> PResult<char> {
    let c = input
        .next_token()
        .ok_or_else(|| ErrMode::from_error_kind(input, ErrorKind::Token))?;
    if c != '0' {
        return Err(ErrMode::from_error_kind(input, ErrorKind::Verify));
    }
    Ok(c)
}

// Using `any` and `verifty`
pub fn parse_prefix_2(input: &mut &str) -> PResult<char> {
    any.verify(|c| *c == '0').parse_next(input)
}

// Using `Parser` implemented for `char`
pub fn parse_prefix_3(input: &mut &str) -> PResult<char> {
    '0'.parse_next(input)
}

//
// String slice parsers
//

// Using core components
pub fn parse_prefix_4<'s>(input: &mut &'s str) -> PResult<&'s str> {
    let expected = "0x";
    if input.len() < expected.len() {
        return Err(ErrMode::from_error_kind(input, ErrorKind::Slice));
    }
    let actual = input.next_slice(expected.len());
    if actual != expected {
        return Err(ErrMode::from_error_kind(input, ErrorKind::Verify));
    }
    Ok(actual)
}

// Using `Parser` implemented for string slices
pub fn parse_prefix_5<'s>(input: &mut &'s str) -> PResult<&'s str> {
    "0x".parse_next(input)
}

//
// Character classes
//

// Using `one_of`
fn parse_hex_digit(input: &mut &str) -> PResult<char> {
    one_of(('0'..='9', 'a'..='f', 'A'..='F')).parse_next(input)
}

// Using `take_while`
fn parse_hex_digits_1<'s>(input: &mut &'s str) -> PResult<&'s str> {
    take_while(1.., ('0'..='9', 'a'..='f', 'A'..='F')).parse_next(input)
}

// Using `hex_digit1`
fn parse_hex_digits_2<'s>(input: &mut &'s str) -> PResult<&'s str> {
    hex_digit1.parse_next(input)
}

fn main() {
    println!("{} - parse prefix ('0')", env!("CARGO_BIN_NAME"));

    let mut input = "0x1a2b Hello";

    let output = parse_prefix_1(&mut input).unwrap();

    assert_eq!(input, "x1a2b Hello");
    assert_eq!(output, '0');

    let mut input = "0x1a2b Hello";

    let output = parse_prefix_2(&mut input).unwrap();

    assert_eq!(input, "x1a2b Hello");
    assert_eq!(output, '0');

    let mut input = "0x1a2b Hello";

    let output = parse_prefix_3(&mut input).unwrap();

    assert_eq!(input, "x1a2b Hello");
    assert_eq!(output, '0');

    let mut input = "0x1a2b Hello";

    let output = parse_prefix_4(&mut input).unwrap();

    assert_eq!(input, "1a2b Hello");
    assert_eq!(output, "0x");

    let output = parse_hex_digit(&mut input).unwrap();

    assert_eq!(input, "a2b Hello");
    assert_eq!(output, '1');

    assert!(parse_hex_digit.parse_next(&mut "Z").is_err());

    let output = parse_hex_digits_1(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(output, "a2b");

    let mut input = "ef1ahij";

    let output = parse_hex_digits_2(&mut input).unwrap();

    assert_eq!(input, "hij");
    assert_eq!(output, "ef1a");
}
