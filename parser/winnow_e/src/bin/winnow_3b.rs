use std::str::from_utf8;

use winnow::ascii::{line_ending, newline, tab};
use winnow::prelude::*;

// Line endings
fn parse_line_ending<'s>(input: &mut &'s str) -> PResult<&'s str> {
    line_ending.parse_next(input)
}

// Newline character
fn parse_newline<'s>(input: &mut &'s str) -> PResult<char> {
    newline.parse_next(input)
}

// Tab character
fn parse_tab<'s>(input: &mut &'s str) -> PResult<char> {
    tab.parse_next(input)
}

fn main() {
    println!("{} - common parsers", env!("CARGO_BIN_NAME"));

    let bytes = [b'\r', b'\n', b'\n', b'\n', b'\t', b'a'];
    let mut input = match from_utf8(&bytes) {
        Ok(input) => input,
        Err(error) => panic!("Invalid UTF-8 sequence: {}", error),
    };

    let output = parse_line_ending(&mut input).unwrap();
    assert_eq!(output, "\r\n");

    let output = parse_line_ending(&mut input).unwrap();
    assert_eq!(output, "\n");

    let output = parse_newline(&mut input).unwrap();
    assert_eq!(output, '\n');

    let output = parse_tab(&mut input).unwrap();
    assert_eq!(output, '\t');
}
