use winnow::PResult;

pub fn do_nothing_parser<'s>(_input: &mut &'s str) -> PResult<&'s str> {
    Ok("")
}

fn main() {
    println!("{} - do nothing parser", env!("CARGO_BIN_NAME"));

    let mut input = "0x1a2b Hello";

    let output = do_nothing_parser(&mut input).unwrap();

    assert_eq!(input, "0x1a2b Hello");
    assert_eq!(output, "");
}
