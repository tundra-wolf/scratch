use std::fmt::{Debug, Display, Formatter, Result as FmtResult};
use std::str::FromStr;

use winnow::ascii::{digit1 as digits, multispace0 as multispaces};
use winnow::combinator::{
    alt, delimited, dispatch, fail, fold_repeat, peek, preceded, repeat, terminated,
};
use winnow::prelude::*;
use winnow::stream::*;
use winnow::token::{any, one_of};

#[derive(Clone, Debug)]
pub enum Expr {
    Value(i64),
    Add(Box<Expr>, Box<Expr>),
    Sub(Box<Expr>, Box<Expr>),
    Mul(Box<Expr>, Box<Expr>),
    Div(Box<Expr>, Box<Expr>),
    Paren(Box<Expr>),
}

impl Display for Expr {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> FmtResult {
        use Expr::{Add, Div, Mul, Paren, Sub, Value};

        match *self {
            Value(value) => write!(fmt, "{}", value),
            Add(ref left, ref right) => write!(fmt, "{} + {}", left, right),
            Sub(ref left, ref right) => write!(fmt, "{} - {}", left, right),
            Mul(ref left, ref right) => write!(fmt, "{} * {}", left, right),
            Div(ref left, ref right) => write!(fmt, "{} / {}", left, right),
            Paren(ref expr) => write!(fmt, "({})", expr),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Token {
    Value(i64),
    Oper(Oper),
    OpenParen,
    CloseParen,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Oper {
    Add,
    Sub,
    Mul,
    Div,
}

impl ContainsToken<Token> for Token {
    #[inline(always)]
    fn contains_token(&self, token: Token) -> bool {
        *self == token
    }
}

impl ContainsToken<Token> for &'_ [Token] {
    #[inline]
    fn contains_token(&self, token: Token) -> bool {
        self.iter().any(|t| *t == token)
    }
}

impl<const LEN: usize> ContainsToken<Token> for &'_ [Token; LEN] {
    #[inline]
    fn contains_token(&self, token: Token) -> bool {
        self.iter().any(|t| *t == token)
    }
}

impl<const LEN: usize> ContainsToken<Token> for [Token; LEN] {
    #[inline]
    fn contains_token(&self, token: Token) -> bool {
        self.iter().any(|t| *t == token)
    }
}

pub fn expr2(i: &mut &str) -> PResult<Expr> {
    let tokens = lex.parse_next(i)?;
    expr.parse_next(&mut tokens.as_slice())
}

pub fn lex(i: &mut &str) -> PResult<Vec<Token>  > {
    preceded(multispaces, repeat(1.., terminated(token, multispaces))).parse_next(i)
}

pub fn token(i: &mut &str) -> PResult<Token> {
    dispatch! {
        peek(any);
        '0' ..= '9' => digits.try_map(FromStr::from_str).map(Token::Value),
        '(' => '('.value(Token::OpenParen),
        ')' => ')'.value(Token::CloseParen),
        '+' => '+'.value(Token::Oper(Oper::Add)),
        '-' => '-'.value(Token::Oper(Oper::Sub)),
        '*' => '*'.value(Token::Oper(Oper::Mul)),
        '/' => '/'.value(Token::Oper(Oper::Div)),
        _ => fail,
    }
    .parse_next(i)
}

pub fn expr(i: &mut &[Token]) -> PResult<Expr> {
    let init = term.parse_next(i)?;

    fold_repeat(
        0..,
        (one_of([Token::Oper(Oper::Add), Token::Oper(Oper::Sub)]), term),
        move || init.clone(),
        |acc, (op, val): (Token, Expr)| {
            if op == Token::Oper(Oper::Add) {
                Expr::Add(Box::new(acc), Box::new(val))
            }
            else {
                Expr::Sub(Box::new(acc), Box::new(val))
            }
        },
    )
    .parse_next(i)
}

fn term(i: &mut &[Token]) -> PResult<Expr> {
    let init = factor.parse_next(i)?;

    fold_repeat(
        0..,
        (one_of([Token::Oper(Oper::Mul), Token::Oper(Oper::Div)]), factor),
        move || init.clone(),
        |acc, (op, val): (Token, Expr)| {
            if op == Token::Oper(Oper::Mul) {
                Expr::Mul(Box::new(acc), Box::new(val))
            }
            else {
                Expr::Div(Box::new(acc), Box::new(val))
            }
        },
    )
    .parse_next(i)
}

fn factor(i: &mut &[Token]) -> PResult<Expr> {
    alt((
        one_of(|t| matches!(t, Token::Value(_))).map(|t| match t {
            Token::Value(v) => Expr::Value(v),
            _ => unreachable!(),
        }),
        parens,
    ))
    .parse_next(i)
}

fn parens(i: &mut &[Token]) -> PResult<Expr> {
    delimited(one_of(Token::OpenParen), expr, one_of(Token::CloseParen))
        .map(|e| Expr::Paren(Box::new(e)))
        .parse_next(i)
}

fn main() {}

#[test]
fn lex_test() {
    let input = "3";
    let expected = Ok(String::from(r#"("", [Value(3)])"#));
    assert_eq!(lex.parse_peek(input).map(|e| format!("{e:?}")), expected);

    let input = "       24          ";
    let expected = Ok(String::from(r#"("", [Value(24)])"#));
    assert_eq!(lex.parse_peek(input).map(|e| format!("{e:?}")), expected);
}
