use winnow::combinator::{opt, terminated};
use winnow::{PResult, Parser};
use winnow_e::parse_as_usize;

// Using `opt` and `terminated`
fn parse_list(input: &mut &str) -> PResult<Vec<usize>> {
    let mut list = Vec::new();

    while let Some(output) =
        opt(terminated(parse_as_usize, opt(','))).parse_next(input)?
    {
        list.push(output);
    }
    Ok(list)
}

fn main() {
    println!("{} - repetition", env!("CARGO_BIN_NAME"));

    let mut input = "0x12ab,0xabcd,0x5cef Hello";

    let digits = parse_list.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(digits, vec![0x12ab, 0xabcd, 0x5cef]);
}
