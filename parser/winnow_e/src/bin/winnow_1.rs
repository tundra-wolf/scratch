fn main() {
    println!("{} - iterator combinators", env!("CARGO_BIN_NAME"));

    let data = vec![1, 2, 3, 4];
    let even_count = data.iter().copied().filter(|d| d % 2 == 0).count();

    assert_eq!(even_count, 2);
}
