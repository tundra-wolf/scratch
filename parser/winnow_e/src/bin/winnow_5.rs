use winnow::ascii::digit1;
use winnow::{PResult, Parser};
use winnow_e::parse_as_usize;

// Using `parse_to`
fn parse_digits_1(input: &mut &str) -> PResult<usize> {
    digit1.parse_to().parse_next(input)
}

fn main() {
    println!("{} - custom output type", env!("CARGO_BIN_NAME"));

    let mut input = "1024 Hello";

    let output = parse_digits_1.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(output, 1024);

    assert!(parse_digits_1(&mut "Z").is_err());

    let mut input = "0d1024 Hello";

    let output = parse_as_usize.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(output, 1024);

    let mut input = "0o1074 Hello";

    let output = parse_as_usize.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(output, 572);

    let mut input = "0b10101100111000 Hello";

    let output = parse_as_usize.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(output, 11064);

    let mut input = "0xba3f Hello";

    let output = parse_as_usize.parse_next(&mut input).unwrap();

    assert_eq!(input, " Hello");
    assert_eq!(output, 47679);

    assert!(parse_as_usize(&mut "Z").is_err());
}
